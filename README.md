# Ansible Role: doctl

This role installs [doctl](https://github.com/digitalocean/doctl) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    doctl_version: v1.70.0 # tag "latest" if you want install the latest version
    doctl_arch: amd64 # amd64, 386, arm64

    setup_dir: /tmp

    doctl_bin_path: /usr/local/bin/doctl
    doctl_repo_path: https://github.com/digitalocean/doctl/releases/download

This role can install the latest version of a specific version. See [available doctl releases](https://github.com/digitalocean/doctl/releases/), and change this variable accordingly.

    doctl_version: v1.70.0 # tag "latest" if you want install the latest version

The path to the home doctl directory.

    doctl_bin_path: /usr/local/bin/doctl

doctl supports amd64, 386 and arm64 CPU architectures, just change for the main architecture of your CPU.

    doctl_arch: amd64 # amd64, 386, arm64

The path of the repository of doctl.

    doctl_repo_path: https://github.com/digitalocean/doctl/releases/download

doctl needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install doctl. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: doctl

## License

MIT / BSD
